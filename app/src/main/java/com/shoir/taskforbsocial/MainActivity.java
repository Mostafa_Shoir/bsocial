package com.shoir.taskforbsocial;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.shoir.taskforbsocial.adapter.ListLocationAdapter;
import com.shoir.taskforbsocial.mapUtils.mapmodel.LocationModel;
import com.shoir.taskforbsocial.mapUtils.util.Constants;
import com.shoir.taskforbsocial.util.JsonParser;
import com.shoir.taskforbsocial.util.Preferences;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Preferences.getInstance().InitializePreferences(this);
        initializeView();
        setListener();

        // get list of location from preferences
        locationModels = JsonParser.getInstance().ParseToLocationInItemsList(Preferences.getInstance().getLocationItems());
        initializeListLocation();
    }

    Button btnSaveLocation_activityMain;
    ListView listView_activityMain;
    LinearLayout layout_content_listView_locations;
    TextView tvGetDirections_activityMain, tvNoLocationAdded_activityMain;


    private void initializeView() {
        btnSaveLocation_activityMain = findViewById(R.id.btnSaveLocation_activityMain);
        listView_activityMain = findViewById(R.id.listView_activityMain);
        layout_content_listView_locations = findViewById(R.id.layout_content_listView_locations);
        tvGetDirections_activityMain = findViewById(R.id.tvGetDirections_activityMain);
        tvNoLocationAdded_activityMain = findViewById(R.id.tvNoLocationAdded_activityMain);
        updateLocationUI();
    }

    ListLocationAdapter listLocationAdapter;
    ArrayList<LocationModel> locationModels = new ArrayList<>();

    void initializeListLocation() {
        if (locationModels != null && locationModels.size() > 0) {
            layout_content_listView_locations.setVisibility(View.VISIBLE);
            tvNoLocationAdded_activityMain.setVisibility(View.GONE);
            listLocationAdapter = new ListLocationAdapter(MainActivity.this, locationModels);
            listView_activityMain.setAdapter(listLocationAdapter);
        } else {
            // if don't have location show hint to tell user (no location added)
            layout_content_listView_locations.setVisibility(View.GONE);
            tvNoLocationAdded_activityMain.setVisibility(View.VISIBLE);
        }
    }

    private void setListener() {
        btnSaveLocation_activityMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isGetLocation = true;
                updateLocationUI();
                if (!mLastKnownLocation.hasAltitude()) {
                    Toast.makeText(MainActivity.this, R.string.Altitude_not_found, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    void saveLocationToPreferences(LocationModel locationModel) {
        boolean found = false;
        if (locationModels != null && locationModels.size() > 0) {
            for (int i = 0; i < locationModels.size(); i++) {
                if (locationModel.getLat().equals(locationModels.get(i).getLat())
                        && locationModel.getLng().equals(locationModels.get(i).getLng())) {
                    Toast.makeText(this, getString(R.string.location_exists), Toast.LENGTH_SHORT).show();
                    locationModels.set(i, locationModel);
                } else {
                    locationModels.add(0, locationModel);
                    Toast.makeText(MainActivity.this, R.string.your_location_has_been_successfully_added, Toast.LENGTH_SHORT).show();
                }
                found = true;
                break;
            }
        } else {
            locationModels = new ArrayList<>();
        }
        if (!found) {
            locationModels.add(0, locationModel);
        }
        //save new location
        String itemsLocation = JsonParser.getInstance().ConvertLocationItemsTojson(locationModels);
        Preferences.getInstance().saveLocationItems(itemsLocation);
    }

    /**
     * Updates the map's UI settings based on whether the user has granted location permission.
     */
    private boolean mLocationPermissionGranted;
    // location retrieved by the Fused Location Provider.
    Location mLastKnownLocation;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    LocationManager locationManager;
    LocationRequest mLocationRequest;
    boolean isPopupGPSShown = false;
    boolean isGetLocation = false;
    String latitude = "", longitude = "", elevation = "";

    private void updateLocationUI() {
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }

        if (mLocationPermissionGranted) {
            locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(100); //100 mill seconds
            mLocationRequest.setFastestInterval(100); //100 mill seconds
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

            LocationServices.getFusedLocationProviderClient(this)
                    .requestLocationUpdates(mLocationRequest, new LocationCallback() {
                        @Override
                        public void onLocationResult(LocationResult locationResult) {
                            super.onLocationResult(locationResult);
                            if (locationResult != null) {
                                mLastKnownLocation = locationResult.getLastLocation();
                                //if my location null so the map at 0.0 location , so move map to this loc
                                tvGetDirections_activityMain.setText(getString(R.string.your_direction) + ": "
                                        + mLastKnownLocation.getLatitude() + ", " + mLastKnownLocation.getLongitude());
                                latitude = String.valueOf(mLastKnownLocation.getLatitude());
                                longitude = String.valueOf(mLastKnownLocation.getLongitude());
                                elevation = String.valueOf(mLastKnownLocation.getAltitude());

                                if (isGetLocation) {
                                    isGetLocation = false;
                                    //if have a location set location model and notify list a adapter
                                    LocationModel locationModel = new LocationModel();
                                    locationModel.setLat(latitude);
                                    locationModel.setLng(longitude);
                                    locationModel.setElevation(elevation);
                                    if (latitude != null && !latitude.equals("") && longitude != null && !longitude.equals("")) {
                                        if (!(locationModels != null && locationModels.size() > 0))
                                            locationModels = new ArrayList<>();
                                        // save location in preferences
                                        saveLocationToPreferences(locationModel);
                                        // notify list a adapter
                                        initializeListLocation();
                                    }
                                }
                            } else {
                                mLastKnownLocation = null;
                            }
                        }
                    }, null);
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                turnGPSOn(mLocationRequest, MainActivity.this);
            }
        }
    }

    BroadcastReceiver GPSStatusChangedBroadCastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String GPSStatus = intent.getExtras().getString("GPSStatus");
            if (GPSStatus.equals("1")) {
                updateLocationUI();
            } else if (GPSStatus.equals("2")) {
                turnGPSOn(mLocationRequest, MainActivity.this);
            }
        }
    };

    public void turnGPSOn(LocationRequest mLocationRequest, final Activity activity) {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        Task<LocationSettingsResponse> result =
                LocationServices.getSettingsClient(activity).checkLocationSettings(builder.build());

        result.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    // All location settings are satisfied. The client can initialize location
                    // requests here.
                } catch (ApiException exception) {
                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the
                            // user a dialog.
                            try {
                                //show popup dialog to turn gps on
                                if (!isPopupGPSShown) {
                                    isPopupGPSShown = true;
                                    // Cast to a resolvable exception.
                                    ResolvableApiException resolvable = (ResolvableApiException) exception;
                                    // Show the dialog by calling startResolutionForResult(),
                                    // and check the result in onActivityResult().
                                    resolvable.startResolutionForResult(activity, 1000);
                                }
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            } catch (ClassCastException e) {
                                // Ignore, should be an impossible error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            break;
                    }
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                } else {
                    Toast.makeText(this, R.string.Location_permission_denied_please_allow_it_from_settings_to_get_your_location, Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onResume() {
        isPopupGPSShown = false;
        LocalBroadcastManager
                .getInstance(this)
                .registerReceiver(
                        GPSStatusChangedBroadCastReceiver,
                        new IntentFilter(
                                Constants.getInstance().LOCAL_PRODCAST_RECIEVER_GPSStatus_Changed));
        super.onResume();
    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(
                GPSStatusChangedBroadCastReceiver);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(
                GPSStatusChangedBroadCastReceiver);
        super.onDestroy();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1000) {
            //location gps popup result
            isPopupGPSShown = false;
            if (resultCode == Activity.RESULT_OK) {
                updateLocationUI();
            }
        }
    }

}
