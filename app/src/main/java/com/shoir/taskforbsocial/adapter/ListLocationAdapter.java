package com.shoir.taskforbsocial.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.shoir.taskforbsocial.MainActivity;
import com.shoir.taskforbsocial.R;
import com.shoir.taskforbsocial.mapUtils.mapmodel.LocationModel;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class ListLocationAdapter extends BaseAdapter {

    ArrayList<LocationModel> locationModels;
    MainActivity activity;
    LayoutInflater mInflater;

    public ListLocationAdapter(MainActivity activity, ArrayList<LocationModel> locationModels) {
        this.activity = activity;
        this.locationModels = locationModels;
    }

    @Override
    public int getCount() {
        return locationModels != null ? locationModels.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return locationModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (mInflater == null)
            mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_list_location_adapter, parent, false);
            holder = new ViewHolder();
            holder.tvLatitude_itemListLocationAdapter = convertView.findViewById(R.id.tvLatitude_itemListLocationAdapter);
            holder.tvLongitude_itemListLocationAdapter = convertView.findViewById(R.id.tvLongitude_itemListLocationAdapter);
            holder.tvElevation_itemListLocationAdapter = convertView.findViewById(R.id.tvElevation_itemListLocationAdapter);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        LocationModel locationModel = locationModels.get(position);
        //set location data
        holder.tvLatitude_itemListLocationAdapter.setText(locationModel.getLat());
        holder.tvLongitude_itemListLocationAdapter.setText(locationModel.getLng());
        if (Double.parseDouble(locationModel.getElevation()) > 1) {
            holder.tvElevation_itemListLocationAdapter.setText(getDecimalFormat(Double.parseDouble(locationModel.getElevation()))
                    + " " + activity.getString(R.string.meters));
        } else {
            holder.tvElevation_itemListLocationAdapter.setText(getDecimalFormat(Double.parseDouble(locationModel.getElevation()))
                    + " " + activity.getString(R.string.meter));
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // get direction on google map
                getDirectionsOnGoogleMap(position);
            }
        });

        return convertView;
    }

    void getDirectionsOnGoogleMap(int position) {
        try {
            double lat, lng;
            String geoUri;
            try {
                lat = Double.parseDouble(locationModels.get(position).getLat());
                lng = Double.parseDouble(locationModels.get(position).getLng());
            } catch (Exception e) {
                lat = 0;
                lng = 0;
            }
            String uri = String.format(Locale.ENGLISH, "geo:%f,%f",
                    lat,
                    lng);
            geoUri = "http://maps.google.com/maps?q=loc:" + lat + "," + lng
                    + " (" + activity.getString(R.string.app_name) + ")";
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
            activity.startActivity(intent);
        } catch (Exception e) {
            Toast.makeText(activity, R.string.invalid_location_data, Toast.LENGTH_SHORT).show();
        }
    }

    public String getDecimalFormat(double number) {
        try {
            DecimalFormat format = (DecimalFormat)
                    NumberFormat.getNumberInstance(Locale.US);
            format.applyPattern("0");
            return format.format(number);
        } catch (Exception e) {
            return "0";
        }
    }

    class ViewHolder {
        TextView tvLatitude_itemListLocationAdapter, tvLongitude_itemListLocationAdapter,
                tvElevation_itemListLocationAdapter;
    }
}
