package com.shoir.taskforbsocial.util;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shoir.taskforbsocial.mapUtils.mapmodel.LocationModel;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class JsonParser {

    static JsonParser instance;

    public static JsonParser getInstance() {
        if (instance == null)
            instance = new JsonParser();
        return instance;
    }

    private JsonParser() {

    }

    public ArrayList<LocationModel> ParseToLocationInItemsList(String locationItemsInJson) {
        try {
            ArrayList<LocationModel> userItems;
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<LocationModel>>() {
            }.getType();
            userItems = gson.fromJson(locationItemsInJson, type);
            return userItems;
        } catch (Exception e1) {
            e1.printStackTrace();
            return null;
        }
    }

    public String ConvertLocationItemsTojson(
            ArrayList<LocationModel> locationItems) {
        try {
            String bodyjson = "";
            Gson gson = new Gson();
            bodyjson = gson.toJson(locationItems);
            return bodyjson;
        } catch (Exception e1) {
            e1.printStackTrace();
            return null;
        }
    }

}
