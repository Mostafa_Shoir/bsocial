package com.shoir.taskforbsocial.util;

import android.content.Context;
import android.content.SharedPreferences;

public class Preferences {
    String PREFS_NAME = "BSOCIALPrefs";
    SharedPreferences appPrefence;
    String locationItems_PREF = "locationItem";


    SharedPreferences.Editor preferenceEditor;
    static Preferences instance;

    public static Preferences getInstance() {
        if (instance == null)
            instance = new Preferences();
        return instance;
    }

    private Preferences() {
    }

    public void InitializePreferences(Context context) {
        appPrefence = context.getSharedPreferences(PREFS_NAME, context.MODE_PRIVATE);
    }

    public String getLocationItems() {
        return appPrefence.getString(locationItems_PREF, "");
    }

    public void saveLocationItems(String locationItem) {
        preferenceEditor = appPrefence.edit();
        preferenceEditor.putString(locationItems_PREF, locationItem);
        preferenceEditor.commit();
    }
}
