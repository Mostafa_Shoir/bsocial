package com.shoir.taskforbsocial.mapUtils.mapmodel;

/**
 * Created by Mostafa on 05/04/2017.
 */

public class OverviewPolylineModel {
    String points;

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }
}
