package com.shoir.taskforbsocial.mapUtils.mapmodel;

/**
 * Created by Mostafa on 05/04/2017.
 */

public class RouteModel {
    String summary;
    String copyrights;
    OverviewPolylineModel overview_polyline;
    BoundModel bounds;

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getCopyrights() {
        return copyrights;
    }

    public void setCopyrights(String copyrights) {
        this.copyrights = copyrights;
    }

    public OverviewPolylineModel getOverview_polyline() {
        return overview_polyline;
    }

    public void setOverview_polyline(OverviewPolylineModel overview_polyline) {
        this.overview_polyline = overview_polyline;
    }

    public BoundModel getBounds() {
        return bounds;
    }

    public void setBounds(BoundModel bounds) {
        this.bounds = bounds;
    }
}
