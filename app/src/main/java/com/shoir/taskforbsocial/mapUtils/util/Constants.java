package com.shoir.taskforbsocial.mapUtils.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.support.v4.app.NotificationCompat;

/**
 * Created by Mostafa on 16/05/2016.
 */
public class Constants {
    static Constants instance;

    private Constants() {
    }

    public static Constants getInstance() {
        if (instance == null)
            instance = new Constants();
        return instance;
    }

    public String LOCAL_PRODCAST_RECIEVER_GPSStatus_Changed = "GPSStatusChanged";

}
