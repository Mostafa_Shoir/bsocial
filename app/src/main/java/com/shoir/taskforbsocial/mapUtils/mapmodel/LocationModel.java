package com.shoir.taskforbsocial.mapUtils.mapmodel;

/**
 * Created by Mostafa on 05/04/2017.
 */

public class LocationModel {
    String lat;
    String lng;
    String elevation;

    public String getElevation() {
        return elevation;
    }

    public void setElevation(String elevation) {
        this.elevation = elevation;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }
}
