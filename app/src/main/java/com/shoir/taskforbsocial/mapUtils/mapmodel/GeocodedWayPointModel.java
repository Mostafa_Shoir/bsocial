package com.shoir.taskforbsocial.mapUtils.mapmodel;

import java.util.ArrayList;

/**
 * Created by Mostafa on 05/04/2017.
 */

public class GeocodedWayPointModel {
    String geocoder_status;
    String place_id;
    ArrayList<String> types;

    public String getGeocoder_status() {
        return geocoder_status;
    }

    public void setGeocoder_status(String geocoder_status) {
        this.geocoder_status = geocoder_status;
    }

    public String getPlace_id() {
        return place_id;
    }

    public void setPlace_id(String place_id) {
        this.place_id = place_id;
    }

    public ArrayList<String> getTypes() {
        return types;
    }

    public void setTypes(ArrayList<String> types) {
        this.types = types;
    }
}
