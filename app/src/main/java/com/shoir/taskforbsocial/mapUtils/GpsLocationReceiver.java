package com.shoir.taskforbsocial.mapUtils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import com.shoir.taskforbsocial.mapUtils.util.Constants;


/**
 * Created by Mostafa shoir on 03/10/2018.
 */

public class GpsLocationReceiver extends BroadcastReceiver implements LocationListener {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
            // react on GPS provider change action
            String GPSStatus = "";
            LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                //GPS turned on
                GPSStatus = "1";
            } else {
                //GPS turned off
                GPSStatus = "2";
            }
            //call broadcast receiver
            Intent bintent = new Intent(
                    Constants.getInstance().LOCAL_PRODCAST_RECIEVER_GPSStatus_Changed);
            bintent.putExtra("GPSStatus", GPSStatus);
            LocalBroadcastManager.getInstance(context).sendBroadcast(bintent);
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
