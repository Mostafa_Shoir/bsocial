package com.shoir.taskforbsocial.mapUtils.mapmodel;

/**
 * Created by Mostafa on 05/04/2017.
 */

public class BoundModel {
    LocationModel northeast;
    LocationModel southwest;

    public LocationModel getNortheast() {
        return northeast;
    }

    public void setNortheast(LocationModel northeast) {
        this.northeast = northeast;
    }

    public LocationModel getSouthwest() {
        return southwest;
    }

    public void setSouthwest(LocationModel southwest) {
        this.southwest = southwest;
    }
}
