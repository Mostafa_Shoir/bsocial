package com.shoir.taskforbsocial.mapUtils.mapmodel;

import java.util.ArrayList;

/**
 * Created by Mostafa on 05/04/2017.
 */

public class DirectionsResponseModel {
    ArrayList<GeocodedWayPointModel> geocoded_waypoints;
    ArrayList<RouteModel> routes;

    public ArrayList<GeocodedWayPointModel> getGeocoded_waypoints() {
        return geocoded_waypoints;
    }

    public void setGeocoded_waypoints(ArrayList<GeocodedWayPointModel> geocoded_waypoints) {
        this.geocoded_waypoints = geocoded_waypoints;
    }

    public ArrayList<RouteModel> getRoutes() {
        return routes;
    }

    public void setRoutes(ArrayList<RouteModel> routes) {
        this.routes = routes;
    }
}
